package carlos_0416.yora.services;

import com.squareup.otto.Bus;

import carlos_0416.yora.yora.infrastructure.YoraApplication;

public class BaseLiveService {
    protected final Bus bus;
    protected final YoraWebService api;
    protected final YoraApplication application;

    public BaseLiveService(YoraApplication application, YoraWebService api) {
        this.bus = application.getBus();
        this.api = api;
        this.application = application;
        bus.register(this);
    }
}
