package carlos_0416.yora.yora.infrastructure;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.TreeMap;

// This class is abstract because we don't want to instantiate the class just the things that inhert
// This class is going to have all of the data that will be present in every single service response errors
public abstract class ServiceResponse {
    private static final String TAG = "ServiceResponse";

    @SerializedName("operationError")
    private String operationError;

    @SerializedName("propertyError")
    private HashMap<String,String> propertyErrors;


    //Operation failed do to invalid state API down, determines weither operationError failed to do
    //critical error
    // Non critical user messed up, critical we have server down etc
    private boolean isCritical;
    private TreeMap<String,String> propertyErrorsCaseInsensative;

    public ServiceResponse(){
        propertyErrors = new HashMap<>();
    }

    public ServiceResponse(String operationError){
        this.operationError = operationError;
    }

    public ServiceResponse(String operationError, boolean isCritical){
        this.operationError = operationError;
        this.isCritical = isCritical;
    }


    public String getOperationError(){
        return operationError;
    }

    public void setOperationError(String operationError){
        this.operationError = operationError;
    }

    public boolean isCritical(){
        return isCritical;
    }

    public void setIsCritical(boolean isCritical){
        this.isCritical=isCritical;
    }

    public void setCriticalError(String criticalError){
        isCritical = true;
        operationError = criticalError;
    }

    public void setPropertyError(String propery, String error){
        propertyErrors.put(propery,error);
    }


    public String getPropertyError(String property){
        if(propertyErrorsCaseInsensative == null || propertyErrorsCaseInsensative.size() != propertyErrors.size()){
            propertyErrorsCaseInsensative = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
            propertyErrorsCaseInsensative.putAll(propertyErrors);
        }

        return propertyErrors.get(property);
    }

    public boolean didSucceed(){
        return (operationError == null || operationError.isEmpty()) && (propertyErrors.size() ==0);
    }

    public void showErrorToast(Context context){
        if(context == null || operationError ==null || operationError.isEmpty())
            return;

        try{
            Toast.makeText(context, operationError, Toast.LENGTH_LONG).show();
        } catch (Exception e){
            Log.e(TAG, "Can't create error toast", e);
        }
    }

}
