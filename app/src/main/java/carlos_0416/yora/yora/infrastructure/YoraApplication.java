package carlos_0416.yora.yora.infrastructure;

import android.app.Application;
import android.net.Uri;

import com.squareup.otto.Bus;

import carlos_0416.yora.services.Module;

/*** The class this is going to extend is called application*/
/*A bag for all of the singltons avoids static*/

//Makes the bus, sets the bus, and makes the bus available to all the activityes and fragments
public class YoraApplication extends Application{
    public static final Uri API_ENDPOINT = Uri.parse("http://yora-playground.3dbuzz.com");
    public static final String STUDENT_TOKEN = "be73fd04bd23446db4b797b4237fb571";

    private Auth auth;
    private Bus bus;

    public YoraApplication(){
        bus = new Bus();
    }



    @Override
    public void onCreate(){
        super.onCreate();
        auth = new Auth(this);
        Module.register(this);
    }

    public Auth getAuth(){
        return  auth;
    }

    public Bus getBus(){
        return bus;
    }


}


