package carlos_0416.yora.dialogs;

import android.app.DialogFragment;
import android.os.Bundle;

import com.squareup.otto.Bus;

import carlos_0416.yora.yora.infrastructure.ActionScheduler;
import carlos_0416.yora.yora.infrastructure.YoraApplication;

public abstract class BaseDialogFragment extends DialogFragment {
    protected YoraApplication application;
    protected Bus bus;
    protected ActionScheduler scheduler;

    @Override
    public void onCreate(Bundle savedState){
        super.onCreate(savedState);
        application = (YoraApplication) getActivity().getApplication();
        scheduler = new ActionScheduler(application);
        scheduler = new ActionScheduler(application);

        bus = application.getBus();
        bus.register(this);


    }
    @Override
    public void onPause(){
        super.onPause();
        scheduler.onPause();
    }

    @Override
    public void onResume(){
        super.onResume();
        scheduler.onResume();
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        bus.unregister(this);
    }


}
