package carlos_0416.yora.services;


// grouping purposes the classes job
// Think about these methods in terms of events
// There are two events
// This class is nothing more then a wrapper around other classes so a million names spaces are not required
import android.net.Uri;

import carlos_0416.yora.yora.infrastructure.ServiceResponse;
import carlos_0416.yora.yora.infrastructure.User;

public final class Account {
    private Account(){
    }

    public static class UserResponse extends ServiceResponse{
        public int id;
        public String AvatarUrl;
        public String DisplayName;
        public String Email;
        public String AuthToken;
        public String UserName;
        public boolean HasPassword;
    }

    public static class LoginWithUsernameRequest{
        public String Username;
        public String Password;

        public LoginWithUsernameRequest(String password, String username) {
            Password = password;
            Username = username;
        }
    }


    //Not needed for maingo
    public static class LoginWithUsernameResponse extends ServiceResponse{
    }


    // When a user shuts his phone down, this will keep his info
    public static class LoginWithLocalTokenRequest {
        public String AuthToken;

        public LoginWithLocalTokenRequest(String authToken) {
            AuthToken = authToken;
        }
    }

    public static class LoginWithLocalTokenResponse extends UserResponse{

    }


    //not needed for maingo....
    public static class LoginWithExternalTokenRequest{
        public String Provider;
        public String Token;
        public String ClientId;

        public LoginWithExternalTokenRequest(String provider, String token) {
            Provider = provider;
            Token = token;
            ClientId = "android";
        }
    }


    public static class LoginWithExternalTokenResponse  extends UserResponse{

    }

    public static class RegisterRequest{
        public String UserName;
        public String Email;
        public String Password;
        public String CleintId;


        public RegisterRequest(String userName, String email, String password) {
            UserName = userName;
            Email = email;
            Password = password;
            CleintId = "android";
        }
    }

    public static class RegisterResponse extends UserResponse{

    }

    public static class RegisterWithExternalTokenRequest{
        public String UserName;
        public String Email;
        public String Provider;
        public String Token;
        public String ClientId;

        public RegisterWithExternalTokenRequest(String userName, String email, String provider, String token) {
            UserName = userName;
            Email = email;
            Provider = provider;
            Token = token;
            ClientId = "android";
        }
    }


    public static class RegisterWithExternalTokenResponse extends UserResponse{

    }



    // This event which will correspond to the parameter list of a method
    public static class ChangeAvatarRequest{
        public Uri NewAvatarUri;

        public ChangeAvatarRequest(Uri newAvatarUri){
            NewAvatarUri = newAvatarUri;
        }
    }


    // This event which will correspond to return of a method
    public static class ChangeAvatarResponse extends ServiceResponse{
        public String avatarUrl;
    }

    public static class UpdateProfileRequest{
        public String DisplayName;
        public String Email;

        public UpdateProfileRequest(String displayName, String email){
            DisplayName = displayName;
            Email = email;
        }
    }


    public static class UpdateProfileResponse extends ServiceResponse{
        public String DisplayName;
        public String Email;
    }

    public static class ChangePasswordRequest{
        public String CurrentPassword;
        public String NewPassword;
        public String ConfirmNewPassword;

        public ChangePasswordRequest(String currentPassword, String newPassword, String confirmNewPassword){
            CurrentPassword = currentPassword;
            NewPassword = newPassword;
            this.ConfirmNewPassword = confirmNewPassword;
        }

    }

    public static class ChangePasswordResponse extends ServiceResponse{
    }

    public static class UserDetailsUpdatedEvent {
        public User User;

        public UserDetailsUpdatedEvent(carlos_0416.yora.yora.infrastructure.User user) {
            User = user;
        }
    }

    public static class UpdateGcmRegistrationRequest{
            public String RegistrationId;

            public UpdateGcmRegistrationRequest(String registrationId){
                RegistrationId = registrationId;
            }
        }

        public static class UpdateGcmRegistrationResponse extends ServiceResponse{

        }
    }

