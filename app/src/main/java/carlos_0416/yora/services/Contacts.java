package carlos_0416.yora.services;

import java.util.List;

import carlos_0416.yora.services.entities.ContactRequest;
import carlos_0416.yora.services.entities.UserDetails;
import carlos_0416.yora.yora.infrastructure.ServiceResponse;

//A container for other classes
public final class Contacts {
    private Contacts(){

    }

    //Used to get a ContactRequest weither from us or someone else
    public static class GetContactRequestsRequest{
        public boolean FromUs;

        public GetContactRequestsRequest(boolean fromUs) {
            FromUs = fromUs;
        }
    }

    //Service Response gives info about stuff about errors
    //Static to allow insatuation of these classes Nested Class
    public static class GetContactRequestsResponse extends ServiceResponse {
        public List<ContactRequest> Requests;
    }

    public static class GetContactsRequest{
        //Do we want to include people we have sent a contact request to but have not heard back yet.
        public boolean IncludePendingContacts;

        public GetContactsRequest(boolean includePendingContacts) {
            IncludePendingContacts = includePendingContacts;
        }
    }

    public static class GetContactsResponse extends ServiceResponse{
        public List<UserDetails> Contacts;
    }


    //When you wanna send a contacts request to somebody
    public static class SendContactRequestRequest extends ServiceResponse{
        public int UserId;

        public SendContactRequestRequest(int userId) {
            UserId = userId;
        }
    }

    //Search for a user and send request to a user
    public static class SendContactRequestResponse extends ServiceResponse{

    }

    public static class RespondToContactRequestRequest{
        public int ContactRequestId;
        public boolean Accept;

        public RespondToContactRequestRequest(int contactRequestId, boolean accept) {
            ContactRequestId = contactRequestId;
            Accept = accept;
        }
    }

    public static class RespondToContactRequestResponse extends ServiceResponse{

    }

    public static class RemoveContactRequest{
        public int ContactId;

        public RemoveContactRequest(int contatctid){
    ContactId = contatctid;
        }
    }

    public static class RemoveContactResponse extends ServiceResponse{
        public int RemovedContactId;
    }

    public static class SearchUsersRequest{
        public String Query;

        public SearchUsersRequest(String query){
            Query = query;
        }
    }

    public static class SearchUsersResponse extends ServiceResponse{
        public List<UserDetails> Users;
        public String Query;
    }
}
