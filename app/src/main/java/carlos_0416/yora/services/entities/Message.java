package carlos_0416.yora.services.entities;

//Responsible for everything a message can have

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Message implements Parcelable {
    private int id;
    private Calendar createdAt;
    private String shortMessage;
    private String longMessage;
    private String imageUrl;
    private UserDetails otherUsers;
    private boolean isFromUs;
    private boolean isRead;
    private boolean isSelected;

    public Message(int id,
                   Calendar createdAt,
                   String shortMessage,
                   String longMessage,
                   String imageUrl, UserDetails otherUsers, boolean isFromUs, boolean isRead) {
        this.id = id;
        this.createdAt = createdAt;
        this.shortMessage = shortMessage;
        this.longMessage = longMessage;
        this.imageUrl = imageUrl;
        this.otherUsers = otherUsers;
        this.isFromUs = isFromUs;
        this.isRead = isRead;
    }

    private Message(Parcel in){
        id =in.readInt();
        createdAt = new GregorianCalendar();
        createdAt.setTimeInMillis(in.readLong());
        shortMessage = in.readString();
        longMessage = in.readString();
        imageUrl = in.readString();
        otherUsers = (UserDetails) in.readParcelable(UserDetails.class.getClassLoader());
        isFromUs = in.readByte() ==1;
        isRead = in.readByte() ==1;
    }


    @Override
    public void writeToParcel(Parcel destination, int flags) {
        destination.writeInt(id);
        destination.writeLong(createdAt.getTimeInMillis());
        destination.writeString(shortMessage);
        destination.writeString(longMessage);
        destination.writeString(imageUrl);
        destination.writeParcelable(otherUsers, 0);
        destination.writeByte((byte) (isFromUs ? 1 : 0));
        destination.writeByte((byte) (isRead()?1:0));
    }

    @Override
    public int describeContents() {
        return 0;
    }



    public int getId() {
        return id;
    }

    public Calendar getCreatedAt() {
        return createdAt;
    }

    public String getShortMessage() {
        return shortMessage;
    }

    public String getLongMessage() {
        return longMessage;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public UserDetails getOtherUsers() {
        return otherUsers;
    }

    public boolean isFromUs() {
        return isFromUs;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead){
        this.isRead = isRead;

    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }


    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };
}
