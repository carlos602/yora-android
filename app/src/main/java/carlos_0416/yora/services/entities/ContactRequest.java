package carlos_0416.yora.services.entities;

import java.util.Calendar;

//This entity is responisble for a contact request whether it came from us or not
public class ContactRequest {
    private boolean isFromUs; //If true we sent this contact request, if false they sent it. Used for orders?
    private UserDetails user;
    private Calendar createdAt;

    public ContactRequest(boolean isFromUs, UserDetails user, Calendar createdAt) {
        this.isFromUs = isFromUs;
        this.user = user;
        this.createdAt = createdAt;
    }

    public boolean isFromUs() {
        return isFromUs;
    }

    public UserDetails getUser() {
        return user;
    }

    public Calendar getCreatedAt() {
        return createdAt;
    }

}
