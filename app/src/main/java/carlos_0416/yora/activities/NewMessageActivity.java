package carlos_0416.yora.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import carlos_0416.yora.R;
import carlos_0416.yora.services.entities.Message;
import carlos_0416.yora.services.entities.UserDetails;
import carlos_0416.yora.views.CameraPreview;

//Responsible for getting  reference to a hardware camera, get that reference and then pass  it into the camera preview.
public class NewMessageActivity extends BaseAuthenticatedActivity implements View.OnClickListener, Camera.PictureCallback {
    private static final String TAG = "NewMessageActivity"; //For logging
    private static final String STATE_CAMERA_INDEX = "STATE_CAMERA_INDEX";

    private static final int REQUEST_SEND_MESSAGE = 1;

    public static final String EXTRA_CONTACT = "EXTRA_CONTACT";

    private Camera camera;  //reference to the current camera we are looking at
    private Camera.CameraInfo cameraInfo; // more details on the current camera we are looking at
    private int currentCameraIndex;
    private CameraPreview cameraPreview; // class


    @Override
    protected void onYoraCreate(Bundle savedState) {
        setContentView(R.layout.activity_new_message);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);  //Screen never goes blank

        //activity has been saved and recreated for whatever
        if(savedState != null) {
            currentCameraIndex = savedState.getInt(STATE_CAMERA_INDEX);
        } else{
            currentCameraIndex = 0;
        }

        cameraPreview = new CameraPreview(this);

        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.activity_new_message_frame);
        //There will be buttons in the future and we want this view to appear before the buttons
        frameLayout.addView(cameraPreview, 0);

        findViewById(R.id.activity_new_message_switch_camera).setOnClickListener(this);
        findViewById(R.id.activity_new_message_takePicture).setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        int id = view.getId();

        if(id == R.id.activity_new_message_takePicture){
            takePicture();
        } else if (id ==R.id.activity_new_message_switch_camera){
            switchCamera();
        }
    }

    private void switchCamera(){
        currentCameraIndex = currentCameraIndex +1 <Camera.getNumberOfCameras() ? currentCameraIndex +1:0;
        establishCamera();
    }

    private void takePicture(){
        camera.takePicture(null, null, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        // method responsible for getting a reference to a camera from the OS
        establishCamera();
    }


    //We have a camera hookup and we are looking at is
    //If this activity is paused release the camera and set the activity to blank
    //That way is a user opens another activity he can use the camera!
    @Override
    protected void onPause() {
        super.onPause();
        if(camera!= null){
            cameraPreview.setCamera(null,null);
            camera.release();
            camera =null;
        }
    }

    //saving the current camera index into the bundle so if the user exits out we know what camera
    //we are looking at
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_CAMERA_INDEX, currentCameraIndex);
    }

    //invoked on onResume
    private void establishCamera(){
        if(camera!=null){
            cameraPreview.setCamera(null,null);
            camera.release();
            camera = null;
        }

        try{
            camera = Camera.open(currentCameraIndex);
        } catch (Exception e){
            Log.e(TAG,"Could not open camera" + currentCameraIndex,e);
            Toast.makeText(this,"Error establishing camera!",Toast.LENGTH_LONG).show();
        }

        cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(currentCameraIndex, cameraInfo);
        cameraPreview.setCamera(camera, cameraInfo);

        if(cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK){
            Toast.makeText(this,"Using back camera",Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this,"Using front camera",Toast.LENGTH_LONG).show();
        }

    }


    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Bitmap bitmap= processBitmap(data);

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,70,output);

        File outputFile = new File(getCacheDir(), "temp-image");
        outputFile.delete();
        try{
            FileOutputStream fileOutput = new FileOutputStream(outputFile);
            fileOutput.write(output.toByteArray());
            fileOutput.close();
        } catch (Exception e){
            Log.e(TAG, "Could not save bitmap", e);
            Toast.makeText(this,"Could not save image to temp directory",Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, SendMessageActivity.class);
        intent.putExtra(SendMessageActivity.EXTRA_IMAGE_PATH, Uri.fromFile(outputFile));

        //If the person who started the new message activity sent in a contact that needed to be the recipient of this message
        //then take that and progate it to the new activity the sent message activity
        UserDetails user = getIntent().getParcelableExtra(EXTRA_CONTACT);
        if (user !=null){
            intent.putExtra(SendMessageActivity.EXTRA_CONTACT,user);
        }

        startActivityForResult(intent,REQUEST_SEND_MESSAGE);
        bitmap.recycle();
    }

    @SuppressWarnings("SuspiciousNameCombination")
    private Bitmap processBitmap(byte[] data) {
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

        if(bitmap.getWidth()>SendMessageActivity.MAX_IMAGE_HEIGHT){
            float scale = (float) SendMessageActivity.MAX_IMAGE_HEIGHT/bitmap.getWidth();
            int finalWidth = (int) (bitmap.getHeight() *scale);
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, SendMessageActivity.MAX_IMAGE_HEIGHT,finalWidth,false);

            if(resizedBitmap != bitmap){
                bitmap.recycle();
                bitmap = resizedBitmap;
            }
        }


        // This matrix represents the changes we want to make to an image
        Matrix matrix = new Matrix();
        if(cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT){
            Matrix matrixMirror = new Matrix();
            matrixMirror.setValues(new float[]{
                    -1,0,0,
                    0,1,0,
                    0,0,1
            });

            matrix.postConcat(matrixMirror);
        }

        matrix.postRotate(90);
        Bitmap processedBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,true);

        if(bitmap!= processedBitmap){
            bitmap.recycle();
        }

        return processedBitmap;


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode ==REQUEST_SEND_MESSAGE && resultCode ==RESULT_OK){
            setResult(RESULT_OK);
            finish();

            Message message = data.getParcelableExtra(SendMessageActivity.RESULT_MESSAGE);

            Intent intent = new Intent(this,MessageActivity.class);
            intent.putExtra(MessageActivity.EXTRA_MESSAGE,message);
            startActivity(intent);
        }
    }
}
